package com.example.demo.config;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;

@Service
public class FBInitialize {


    @Value("${app-firebase.file.config}")
    private String firebaseConfig;

    @PostConstruct
    public void initialize() {
        try {
            //FileInputStream serviceAccount = new FileInputStream("/home/tuan/Desktop/Intellij/firebase/src/main/java/com/example/demo_firebase/firebase-adminsdk.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseConfig).getInputStream()))
                    .setDatabaseUrl("https://fir-7fc16.firebaseio.com")
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}