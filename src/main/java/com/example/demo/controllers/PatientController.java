package com.example.demo.controllers;

import com.example.demo.models.Patient;
import com.example.demo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping("/patients")
public class PatientController {

    @Autowired
    PatientService patientService;

    @GetMapping()
    public Patient getPatient(@RequestParam String name ) throws InterruptedException, ExecutionException {
        return patientService.getPatientDetails(name);
    }

    @PostMapping()
    public String createPatient(@RequestBody Patient patient ) throws InterruptedException, ExecutionException {
        return patientService.savePatientDetails(patient);
    }

    @PutMapping()
    public String updatePatient(@RequestBody Patient patient  ) throws InterruptedException, ExecutionException {
        return patientService.updatePatientDetails(patient);
    }

    @DeleteMapping()
    public String deletePatient(@RequestParam String name){
        return patientService.deletePatient(name);
    }
}
